package com.intrans.timeli.data.test;

import com.intrans.timeli.data.test.handlers.AsyncCallBack;
import com.intrans.timeli.data.test.handlers.Callback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SnowPlowJob {

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private SnowPlowHistoricalData data;

    @PostConstruct
    public void runOnStartup() {
        executeArchiveSnowPlowData();
    }

    /**
     * Archives Snow Plow AVL data. Update Frequency : 1 min
     */
    @Scheduled(cron = "${job.schedule.snowplow.cron}")
    public void executeArchiveSnowPlowData() {
        new AsyncCallBack(new Callback() {

            @Override
            public void call() {
                try {
                    System.out.println("Strating to get historical data");
                    data.downloadHistoricalSnowPlow();
                } catch (Exception e) {
                    System.out.println("Data archiving failed for snow plow data.");
                    e.printStackTrace();
                }

            }
        }).execute(taskExecutor);
    }

}
