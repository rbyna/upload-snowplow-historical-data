package com.intrans.timeli.data.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigConstants {

    public String getSnowPlowArchiveRoot() {
        return snowPlowArchiveRoot;
    }

    public void setSnowPlowArchiveRoot(String snowPlowArchiveRoot) {
        this.snowPlowArchiveRoot = snowPlowArchiveRoot;
    }

    public String getSnowPlowFNPrefix() {
        return snowPlowFNPrefix;
    }

    public void setSnowPlowFNPrefix(String snowPlowFNPrefix) {
        this.snowPlowFNPrefix = snowPlowFNPrefix;
    }

    @Value("${archive.snowplow.root.folder}")
    private String snowPlowArchiveRoot;

    @Value("${archive.file.snowplow.name}")
    private String snowPlowFNPrefix;
}
