package com.intrans.timeli.data.test;

import com.intrans.timeli.data.test.skyhawk.domain.SnowPlow;
import com.intrans.timeli.data.test.skyhawk.repo.SnowPlowRepository;
import com.intrans.timeli.data.test.utils.CSVUtils;
import com.intrans.timeli.data.test.utils.DateUtils;
import com.intrans.timeli.data.test.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class SnowPlowHistoricalData {

    @Autowired
    ConfigConstants configConstants;

    @Autowired
    SnowPlowRepository snowPlowDao;

    public void downloadHistoricalSnowPlow() throws IOException, InterruptedException {
        LocalDateTime start = LocalDateTime.of(2019, 3, 9, 0, 0);
        LocalDateTime end = LocalDateTime.of(2019, 3, 10, 0, 0);
        while (start.isBefore(end)) {
            LocalDateTime startN = start.plusMinutes(15);
            archiveSnowPlowData(start, startN);
            start = startN;
            Thread.sleep(500);
        }
    }

    public void archiveSnowPlowData(LocalDateTime start, LocalDateTime end) throws IOException {
        Path toDayDir = Paths.get(configConstants.getSnowPlowArchiveRoot(),
                DateUtils.formatLocalDateTime(start, DateUtils.PATTERN_MM_DD_YYYY));
        if (!Files.exists(toDayDir)) {
            System.out.println("Creating new directory: " + toDayDir);
            FileUtils.makeDirectory(toDayDir);
        }
        // Path toDayDir =
        // FileUtils.getTodayDirectory(configConstants.getSnowPlowArchiveRoot());
        File csvFile = FileUtils.getFileObj(toDayDir.toString(), configConstants.getSnowPlowFNPrefix(), FileUtils.FileType.CSV);
        FileOutputStream fos;
        // At the beginning of the day, create a new file and update with the headers.
        if (!csvFile.exists() || csvFile.isDirectory()) {
            fos = new FileOutputStream(csvFile, true);
            CSVUtils.writeAsCSV(new ArrayList<>(), SnowPlow.class, fos, true);
            fos.close();
        }
        fos = new FileOutputStream(csvFile, true);
        String startStr = DateUtils.formatLocalDateTime(start, DateUtils.PATTERN_YYYY_MM_DD_HH_MM_SS);
        String endStr = DateUtils.formatLocalDateTime(end, DateUtils.PATTERN_YYYY_MM_DD_HH_MM_SS);
        List<SnowPlow> snowPlows = snowPlowDao.snowPlowExtractAll(startStr, endStr);
        System.out.println(startStr + " \t " + endStr);
        System.out.println(snowPlows.size());
        CSVUtils.writeAsCSV(snowPlows, SnowPlow.class, fos, false);
        fos.close();
    }
}
