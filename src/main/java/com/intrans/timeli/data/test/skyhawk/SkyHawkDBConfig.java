package com.intrans.timeli.data.test.skyhawk;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Database configuration class for the SkyHawk DB
 * 
 * @author krishnaj
 *
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "skyHawkEntityManagerFactory", transactionManagerRef = "skyHawksTransactionManager", basePackages = {
		"com.intrans.timeli.data.test.skyhawk.repo" })
public class SkyHawkDBConfig {

	@Bean(name = "skyHawkDataSource")
	@ConfigurationProperties(prefix = "skyhawk.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "skyHawkEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean skyHawkEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("skyHawkDataSource") DataSource dataSource) {
		return builder.dataSource(dataSource).packages("com.intrans.timeli.data.test.skyhawk.domain")
				.persistenceUnit("skyhawk").build();

	}

	@Bean(name = "skyHawksTransactionManager")
	public PlatformTransactionManager skyHawkTransactionManager(
			@Qualifier("skyHawkEntityManagerFactory") EntityManagerFactory skyHawkEntityManagerFactory) {
		return new JpaTransactionManager(skyHawkEntityManagerFactory);

	}

}
