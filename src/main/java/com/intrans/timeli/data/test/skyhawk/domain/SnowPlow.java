package com.intrans.timeli.data.test.skyhawk.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

@Entity
@EntityScan("com.intrans.timeli.data.test.skyhawk.*")
@NamedStoredProcedureQuery(name = "snowPlowExtractAll", procedureName = "IowaDOTDataExtractionAll", resultClasses = SnowPlow.class, parameters = {
		@StoredProcedureParameter(name = "start", type = String.class, mode = ParameterMode.IN),
		@StoredProcedureParameter(name = "end", type = String.class, mode = ParameterMode.IN) })
@Table(name = "snowplow")
public class SnowPlow implements Serializable {
	private static final long serialVersionUID = 8368702121323799003L;

	@Id
	@Column(name = "LOGID")
	@JsonProperty(value = "LOGID")
	private Long logId;

	@JsonProperty(value = "LABEL")
	@Column(name = "LABEL")
	private String label;

	@JsonProperty(value = "DEVICEID")
	@Column(name = "DEVICEID")
	private String deviceId;

	@JsonProperty(value = "XPOSITION")
	@Column(name = "XPOSITION")
	private Double xPosition;

	@JsonProperty(value = "YPOSITION")
	@Column(name = "YPOSITION")
	private Double yPosition;

	@JsonProperty(value = "HEADING")
	@Column(name = "HEADING")
	private Integer heading;

	@JsonProperty(value = "VELOCITY")
	@Column(name = "VELOCITY")
	private Double velocity;

	@JsonProperty(value = "ROADTEMP")
	@Column(name = "ROADTEMP")
	private Double roadTemp;

	@JsonProperty(value = "AIRTEMP")
	@Column(name = "AIRTEMP")
	private Double airTemp;

	@JsonProperty(value = "SOLIDMATERIAL")
	@Column(name = "SOLIDMATERIAL")
	private Integer solidMaterial;

	@JsonProperty(value = "LIQUIDMATERIAL")
	@Column(name = "LIQUIDMATERIAL")
	private Integer liquidMaterial;

	@JsonProperty(value = "PREWETMATERIAL")
	@Column(name = "PREWETMATERIAL")
	private Integer prewetMaterial;

	@JsonProperty(value = "SOLIDRATE")
	@Column(name = "SOLIDRATE")
	private Integer solidRate;

	@JsonProperty(value = "LIQUIDRATE")
	@Column(name = "LIQUIDRATE")
	private Integer liquidRate;

	@JsonProperty(value = "PREWETRATE")
	@Column(name = "PREWETRATE")
	private Double preWetRate;

	@JsonProperty(value = "FRONTPLOWSTATE")
	@Column(name = "FRONTPLOWSTATE")
	private Integer frontPlowState;

	@JsonProperty(value = "RIGHTWINGPLOWSTATE")
	@Column(name = "RIGHTWINGPLOWSTATE")
	private Integer rightWingPlowState;

	@JsonProperty(value = "LEFTWINGPLOWSTATE")
	@Column(name = "LEFTWINGPLOWSTATE")
	private Integer leftWingPlowState;

	@JsonProperty(value = "UNDERBELLYPLOWSTATE")
	@Column(name = "UNDERBELLYPLOWSTATE")
	private Integer underBellyPlowState;

	@JsonProperty(value = "ODOMETER")
	@Column(name = "ODOMETER")
	private Double odoMeter;

	@JsonProperty(value = "ENGINEHOURS")
	@Column(name = "ENGINEHOURS")
	private Double engineHours;

	// TODO
	@JsonProperty(value = "LOCATION")
	@Column(name = "LOCATION")
	private String location;

	@JsonProperty(value = "SOLIDRUNTOTAL")
	@Column(name = "SOLIDRUNTOTAL")
	private Integer solidRunTotal;

	@JsonProperty(value = "LIQUIDRUNTOTAL")
	@Column(name = "LIQUIDRUNTOTAL")
	private Integer liquidRunTotal;

	@JsonProperty(value = "PREWETRUNTOTAL")
	@Column(name = "PREWETRUNTOTAL")
	private Integer prewetRunTotal;

	@JsonProperty(value = "FIRMWARE")
	@Column(name = "FIRMWARE")
	private String firmware;

	@JsonProperty(value = "SERIALNUMBER")
	@Column(name = "SERIALNUMBER")
	private String serialNumber;

	@JsonProperty(value = "OTHERFIRMWARE")
	@Column(name = "OTHERFIRMWARE")
	private String otherFirmware;

	@JsonProperty(value = "SOLIDSETRATE")
	@Column(name = "SOLIDSETRATE")
	private Integer solidSetRate;

	@JsonProperty(value = "LIQUIDSETRATE")
	@Column(name = "LIQUIDSETRATE")
	private Integer liquidSetRate;

	@JsonProperty(value = "PREWETSETRATE")
	@Column(name = "PREWETSETRATE")
	private Integer prewetSetRate;

	@JsonProperty(value = "BLASTMODE")
	@Column(name = "BLASTMODE")
	private Integer blastMode;

	// TODO
	@JsonProperty(value = "ACCIDENTMODE")
	@Column(name = "ACCIDENTMODE")
	private String accidentMode;

	@JsonProperty(value = "LOGDT")
	@Column(name = "LOGDT")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Chicago")
	private Date logDate;

	@JsonProperty(value = "MODIFIEDDT")
	@Column(name = "MODIFIEDDT")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Chicago")
	private Date modifiedDate;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Double getxPosition() {
		return xPosition;
	}

	public void setxPosition(Double xPosition) {
		this.xPosition = xPosition;
	}

	public Double getyPosition() {
		return yPosition;
	}

	public void setyPosition(Double yPosition) {
		this.yPosition = yPosition;
	}

	public Integer getHeading() {
		return heading;
	}

	public void setHeading(Integer heading) {
		this.heading = heading;
	}

	public Double getVelocity() {
		return velocity;
	}

	public void setVelocity(Double velocity) {
		this.velocity = velocity;
	}

	public Double getRoadTemp() {
		return roadTemp;
	}

	public void setRoadTemp(Double roadTemp) {
		this.roadTemp = roadTemp;
	}

	public Double getAirTemp() {
		return airTemp;
	}

	public void setAirTemp(Double airTemp) {
		this.airTemp = airTemp;
	}

	public Integer getSolidMaterial() {
		return solidMaterial;
	}

	public void setSolidMaterial(Integer solidMaterial) {
		this.solidMaterial = solidMaterial;
	}

	public Integer getLiquidMaterial() {
		return liquidMaterial;
	}

	public void setLiquidMaterial(Integer liquidMaterial) {
		this.liquidMaterial = liquidMaterial;
	}

	public Integer getPrewetMaterial() {
		return prewetMaterial;
	}

	public void setPrewetMaterial(Integer prewetMaterial) {
		this.prewetMaterial = prewetMaterial;
	}

	public Integer getSolidRate() {
		return solidRate;
	}

	public void setSolidRate(Integer solidRate) {
		this.solidRate = solidRate;
	}

	public Integer getLiquidRate() {
		return liquidRate;
	}

	public void setLiquidRate(Integer liquidRate) {
		this.liquidRate = liquidRate;
	}

	public Double getPreWetRate() {
		return preWetRate;
	}

	public void setPreWetRate(Double preWetRate) {
		this.preWetRate = preWetRate;
	}

	public Integer getFrontPlowState() {
		return frontPlowState;
	}

	public void setFrontPlowState(Integer frontPlowState) {
		this.frontPlowState = frontPlowState;
	}

	public Integer getRightWingPlowState() {
		return rightWingPlowState;
	}

	public void setRightWingPlowState(Integer rightWingPlowState) {
		this.rightWingPlowState = rightWingPlowState;
	}

	public Integer getLeftWingPlowState() {
		return leftWingPlowState;
	}

	public void setLeftWingPlowState(Integer leftWingPlowState) {
		this.leftWingPlowState = leftWingPlowState;
	}

	public Integer getUnderBellyPlowState() {
		return underBellyPlowState;
	}

	public void setUnderBellyPlowState(Integer underBellyPlowState) {
		this.underBellyPlowState = underBellyPlowState;
	}

	public Double getOdoMeter() {
		return odoMeter;
	}

	public void setOdoMeter(Double odoMeter) {
		this.odoMeter = odoMeter;
	}

	public Double getEngineHours() {
		return engineHours;
	}

	public void setEngineHours(Double engineHours) {
		this.engineHours = engineHours;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getSolidRunTotal() {
		return solidRunTotal;
	}

	public void setSolidRunTotal(Integer solidRunTotal) {
		this.solidRunTotal = solidRunTotal;
	}

	public Integer getLiquidRunTotal() {
		return liquidRunTotal;
	}

	public void setLiquidRunTotal(Integer liquidRunTotal) {
		this.liquidRunTotal = liquidRunTotal;
	}

	public Integer getPrewetRunTotal() {
		return prewetRunTotal;
	}

	public void setPrewetRunTotal(Integer prewetRunTotal) {
		this.prewetRunTotal = prewetRunTotal;
	}

	public String getFirmware() {
		return firmware;
	}

	public void setFirmware(String firmware) {
		this.firmware = firmware;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getOtherFirmware() {
		return otherFirmware;
	}

	public void setOtherFirmware(String otherFirmware) {
		this.otherFirmware = otherFirmware;
	}

	public Integer getSolidSetRate() {
		return solidSetRate;
	}

	public void setSolidSetRate(Integer solidSetRate) {
		this.solidSetRate = solidSetRate;
	}

	public Integer getLiquidSetRate() {
		return liquidSetRate;
	}

	public void setLiquidSetRate(Integer liquidSetRate) {
		this.liquidSetRate = liquidSetRate;
	}

	public Integer getPrewetSetRate() {
		return prewetSetRate;
	}

	public void setPrewetSetRate(Integer prewetSetRate) {
		this.prewetSetRate = prewetSetRate;
	}

	public Integer getBlastMode() {
		return blastMode;
	}

	public void setBlastMode(Integer blastMode) {
		this.blastMode = blastMode;
	}

	public String getAccidentMode() {
		return accidentMode;
	}

	public void setAccidentMode(String accidentMode) {
		this.accidentMode = accidentMode;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
