package com.intrans.timeli.data.test.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Feb 6, 2017
 *
 */
public class FileUtils extends org.apache.commons.io.FileUtils {

	public enum FileType {
		CSV(".csv"), XML(".xml"), JSON(".json"), GZ(".gz");
		private String extType;

		FileType(String extType) {
			this.extType = extType;
		}

		public String getExtType() {
			return extType;
		}
	}

	public void writeToHdfs() {
		// TODO
	}

	/**
	 * Create directory with given path.
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static Path makeDirectory(Path path) throws IOException {
		return Files.createDirectories(path);
	}

	/**
	 * 
	 * @param parentLocation
	 * @param dirName
	 * @return
	 * @throws IOException
	 */
	public static Path makeDirectory(String parentLocation, String dirName) throws IOException {
		Path path = Paths.get(parentLocation, dirName);
		return makeDirectory(path);
	}

	/**
	 * Saves data into local reference file. This file will be used by the
	 * Controller.
	 * 
	 * @param folderPath
	 * @param fileName
	 * @param obj
	 * @return
	 * @throws IOException
	 */
	public static <T> File archiveDataForInternal(String folderPath, String fileName, T obj) throws IOException {
		Path path = Paths.get(folderPath, fileName + FileType.XML.getExtType());
		File f = path.toFile();
		// Write XML Content
		writeStringToFile(f, TimeliUtils.writeValueAsString(obj));
		return f;
	}

	/**
	 * Returns the today directory path in the given parent folder. If directory
	 * doesn't exist then creates one.
	 * 
	 * @param rootFolder
	 * @return
	 * @throws IOException
	 */
	public static Path getTodayDirectory(String rootFolder) throws IOException {
		Path path = Paths.get(rootFolder, DateUtils.getDefaultCurrentDateStr());
		if (!Files.exists(path)) {
			makeDirectory(path);
		}
		return path;
	}

	/**
	 * Creates a file object from the root folder with the given file name and
	 * extension type.
	 * 
	 * @param rootFolder
	 * @param fileName
	 * @param fileType
	 * @return
	 */
	public static File getFileObj(String rootFolder, String fileName, FileType fileType) {
		Path path = Paths.get(rootFolder, fileName + fileType.getExtType());
		return path.toFile();
	}

}
