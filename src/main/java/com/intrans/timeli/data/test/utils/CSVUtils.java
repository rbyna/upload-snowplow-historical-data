package com.intrans.timeli.data.test.utils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.util.*;
import java.util.Map.Entry;

/**
 * CSV Utililty functions. Reference
 * {@linkplain https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/}
 */
public class CSVUtils {
	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';

	public static List<String> parseLine(String cvsLine) {
		return parseLine(cvsLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
	}

	public static List<String> parseLine(String cvsLine, char separators) {
		return parseLine(cvsLine, separators, DEFAULT_QUOTE);
	}

	public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

		List<String> result = new ArrayList<>();

		// if empty, return!
		if (cvsLine == null || cvsLine.isEmpty()) {
			return result;
		}

		if (customQuote == ' ') {
			customQuote = DEFAULT_QUOTE;
		}

		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}

		StringBuffer curVal = new StringBuffer();
		boolean inQuotes = false;
		boolean startCollectChar = false;
		boolean doubleQuotesInColumn = false;

		char[] chars = cvsLine.toCharArray();

		for (char ch : chars) {

			if (inQuotes) {
				startCollectChar = true;
				if (ch == customQuote) {
					inQuotes = false;
					doubleQuotesInColumn = false;
				} else {

					if (ch == '\"') {
						if (!doubleQuotesInColumn) {
							curVal.append(ch);
							doubleQuotesInColumn = true;
						}
					} else {
						curVal.append(ch);
					}

				}
			} else {
				if (ch == customQuote) {

					inQuotes = true;

					if (chars[0] != '"' && customQuote == '\"') {
						curVal.append('"');
					}

					// double quotes in column will hit this!
					if (startCollectChar) {
						curVal.append('"');
					}

				} else if (ch == separators) {

					result.add(curVal.toString());

					curVal = new StringBuffer();
					startCollectChar = false;

				} else if (ch == '\r') {
					// ignore LF characters
					continue;
				} else if (ch == '\n') {
					// the end, break!
					break;
				} else {
					curVal.append(ch);
				}
			}

		}

		result.add(curVal.toString());
		return result;
	}

	public static CSVParser parseCSVFileWithHeaders(Reader in) throws IOException {
		return CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
	}

	/**
	 * Converts CSV file to the JSON Object.
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static List<Map<String, String>> convertCSVToJSON(Reader in) throws IOException {
		List<Map<String, String>> jsonList = new ArrayList<>();

		CSVParser parser = CSVFormat.DEFAULT.withTrim().withFirstRecordAsHeader().parse(in);
		Map<String, String> headersMap = new HashMap<>();
		// It maps the modified headers with the original headers
		parser.getHeaderMap().keySet().forEach(s -> {
			if (s.startsWith("\uFEFF")) {
				headersMap.put(StringUtils.deleteWhitespace(StringUtils.substring(s, 1)), s);
			} else {
				headersMap.put(StringUtils.deleteWhitespace(s), s);
			}
		});
		Set<Entry<String, String>> headersEntrySet = headersMap.entrySet();
		Iterator<CSVRecord> iterator = parser.iterator();
		Map<String, String> json = null;
		while (iterator.hasNext()) {
			CSVRecord record = iterator.next();
			if (record.isConsistent()) {
				json = new HashMap<>();
				for (Entry<String, String> header : headersEntrySet) {
					json.put(header.getKey(), record.get(header.getValue()));
				}
				jsonList.add(json);
			}

		}
		return jsonList;
	}

	/**
	 * Convert the list of objects into csv format and write them into the file.
	 * 
	 * @param obj
	 * @param valueType
	 * @param out
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static <T> void writeAsCSV(List<T> obj, Class<T> valueType, OutputStream out, boolean useHeader)
			throws JsonGenerationException, JsonMappingException, IOException {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(valueType);
		mapper.writer(schema.withUseHeader(useHeader)).writeValue(out, obj);
	}

	public static <T> List<T> readAsCSV(File src, Class<T> valueType, boolean useHeader)
			throws JsonProcessingException, IOException {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(valueType);
		MappingIterator<T> itr = mapper.readerFor(valueType).with(schema.withUseHeader(useHeader)).readValues(src);
		return itr.readAll();
	}

}
