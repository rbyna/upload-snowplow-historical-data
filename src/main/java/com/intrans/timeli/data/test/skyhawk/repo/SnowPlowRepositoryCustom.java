package com.intrans.timeli.data.test.skyhawk.repo;


import com.intrans.timeli.data.test.skyhawk.domain.SnowPlow;

import java.util.List;

public interface SnowPlowRepositoryCustom {

	List<SnowPlow> snowPlowExtractAll(String start, String end);
}
