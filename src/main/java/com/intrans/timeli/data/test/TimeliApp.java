package com.intrans.timeli.data.test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

/**
 * Wavetronix Main Application class. This class will be invoked by the
 * SpringBoot Initializer
 * 
 * @author Vamsi Krishna J <br />
 *         <b>Date:</b> Apr 4, 2017
 *
 */
@SpringBootApplication(scanBasePackages = "com.intrans.timeli.data.*")
@EnableMongoRepositories(basePackages = { "com.intrans.timeli.data.*" })
@EnableScheduling
@EnableCaching
@EnableAutoConfiguration
public class TimeliApp {

	/**
	 * Will be invoked by spring boot.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(TimeliApp.class, args);
	}

	@Bean
	public JettyEmbeddedServletContainerFactory jettyEmbeddedServletContainerFactory() {
		JettyEmbeddedServletContainerFactory jettyContainer =
				new JettyEmbeddedServletContainerFactory();

		jettyContainer.setPort(8085);
		jettyContainer.setContextPath("/home");
		return jettyContainer;
	}
	/**
	 * Customized {@link RestTemplate} with XML Binding options.
	 * 
	 * @return {@link RestTemplate}
	 */
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		for (int i = 0; i < restTemplate.getMessageConverters().size(); i++) {
			if (restTemplate.getMessageConverters().get(i) instanceof MappingJackson2XmlHttpMessageConverter) {
				restTemplate.getMessageConverters().set(i, mappingJackson2XmlHttpMessageConverter());
			}
		}
		return restTemplate;
	}

	/**
	 * Modified XML Mapper.
	 * 
	 * @return {@link MappingJackson2XmlHttpMessageConverter}
	 */
	@Bean
	public MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter() {
		MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter = new MappingJackson2XmlHttpMessageConverter();
		XmlMapper mapper = new XmlMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mappingJackson2XmlHttpMessageConverter.setObjectMapper(mapper);
		return mappingJackson2XmlHttpMessageConverter;
	}


	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(5);
		taskExecutor.setMaxPoolSize(10);
		return taskExecutor;
	}

}
