package com.intrans.timeli.data.test.skyhawk.repo;

import com.intrans.timeli.data.test.skyhawk.domain.SnowPlow;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.ArrayList;
import java.util.List;

public class SnowPlowRepositoryImpl implements SnowPlowRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<SnowPlow> snowPlowExtractAll(String start, String end) {
		// TODO Auto-generated method stub
		List<SnowPlow> resultset = new ArrayList<>();
		StoredProcedureQuery storedProcQuery = em.createNamedStoredProcedureQuery("snowPlowExtractAll");
		StoredProcedureQuery storedProc = storedProcQuery.setParameter("start", start).setParameter("end", end);
		storedProc.getResultList().forEach(sp -> {
			resultset.add((SnowPlow) sp);
		});
		return resultset;
	}

}
