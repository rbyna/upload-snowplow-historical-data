package com.intrans.timeli.data.test.skyhawk.repo;

import com.intrans.timeli.data.test.skyhawk.domain.SnowPlow;
import org.springframework.data.repository.CrudRepository;

public interface SnowPlowRepository extends CrudRepository<SnowPlow, Long>, SnowPlowRepositoryCustom {

}
