### Upload Snowplow Data

TO run it:

1. Go to SnowPlowHistoricalData#downloadHistoricalSnowPlow()
2. Change start and end time
3. Make sure archive.root.folder=/datadrive4 in application.properties is set correctly.
4. Run: mvn clean install -DskipTests
5. scp jar into Reactor Feeds VM.
6. Run the jar
7. Once the job is complete, stop the process